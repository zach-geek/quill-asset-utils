# Quill Asset Utils

Utilities for integrating Oculus  Quill drawings into 3D frameworks like ue4 or
aframe.

## What's Included

### `QuillTool.ipynb`

A jupyter notebook which makes it a lot easier to automate the Quill export tool
and export quill drawings into one file per layer.

Eventually, I'll extract the functionality into a gem, but for now, this might
be useful to someone else.

### `models/skycube.fbx`

A cube with the UVs mapped to the correct locations for Quill's vertically-
stacked cube map format

### `ue4/Blueprints/FlipBookAnimator.COPY`

I had a lot of trouble getting animations from Quill into UE4. Eventually I
settled on importing each frame as a separate static mesh, and then changing
the static mesh asset source on the object I want to animate every frame.

This is a blueprint which takes care of all that. To use it:

0. Import the `FlipBookAnimator` asset into your project
1. Export your animation in `FBX` format. If you have ruby installed, you can
   use the `QuillTool.ipynb` notebook for extra convenience
2. Create a new folder in the content browser.
3. Drag and drop the `.fbx` file into the new content folder. When importing
   choose to import as **static mesh** (not _skeletal_ or _geometry cache_).
   This will fill up that folder with one static mesh per frame of your
   animation.
4. Now place a `FlipBookAnimator`, then use the `Details` panel to select the
   material you want to use
5. Use the `Details` panel to select any  the static mesh frames that you
   imported. It does not matter which frame.
6. Adjust the frames per second to match what you had in Quill

### `ue4/Materials/`

A bunch of materials that work well with Quill features like directional opacity
